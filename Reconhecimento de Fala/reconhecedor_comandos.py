import speech_recognition as sr
from nltk import word_tokenize, corpus
import json
from GeometricForm import GeometricForm
from GeometricPropertie import GeometricPropertie

INDIOMA_CORPUS = "portuguese"
INDIOMA_FALA = "pt-BR"
CAMINHO_CONFIGURACAO = "config.json"


def iniciar():
    global reconhecedor
    global palavras_de_parada
    global nome_assistente
    global acoes
    global dicionarioValores

    areaTriangulo = GeometricPropertie(
        "área", "base vezes altura divididos por 2")

    areaLosango = GeometricPropertie(
        "área", "diagonal 1 vezes diagonal 2 divididas por 2")
    volumeCubo = GeometricPropertie(
        "volume", "tamanho das arestas ao cubo (elevado a três)")

    triangulo = GeometricForm("triângulo", areaTriangulo)
    losango = GeometricForm("losango", areaLosango)
    cubo = GeometricForm("cubo", volumeCubo)

    dicionarioValores = {
        triangulo.name: triangulo,
        losango.name: losango,
        cubo.name: cubo
    }

    reconhecedor = sr.Recognizer()

    palavras_de_parada = corpus.stopwords.words(INDIOMA_CORPUS)
    palavras_de_parada.append('fórmula')
    palavras_de_parada.remove('qual')

    with open(CAMINHO_CONFIGURACAO, "r", encoding='utf-8') as arquivo_configuracao:
        configuracao = json.load(arquivo_configuracao)

        nome_assistente = configuracao["nome"]
        acoes = configuracao["acoes"]

        arquivo_configuracao.close()


def escutar_comando():
    global reconhecedor

    comando = None

    with sr.Microphone() as fonte_audio:
        reconhecedor.adjust_for_ambient_noise(fonte_audio)

        print("Fale alguma coisa...")
        fala = reconhecedor.listen(fonte_audio)

        try:
            comando = reconhecedor.recognize_google(
                fala, language=INDIOMA_FALA)
        except sr.UnknownValueError:
            pass

    return comando


def eliminar_palavras_de_parada(tokens):
    global palavras_de_parada

    tokens_filtrados = []
    for token in tokens:
        if token not in palavras_de_parada:
            tokens_filtrados.append(token)

    return tokens_filtrados


def tokenizar_comando(comando):
    global nome_assistente

    acao = None
    propriedade = None
    objeto = None

    print(comando)

    tokens = word_tokenize(comando, INDIOMA_CORPUS)
    try:
        if tokens:
            tokens = eliminar_palavras_de_parada(tokens)
            if len(tokens) >= 3:
                if nome_assistente == tokens[0].lower():
                    acao = tokens[1].lower()
                    propriedade = tokens[2].lower()
                    objeto = tokens[3].lower()
    except:
        print("Desculpe houve um engano, poderia perguntar novamente?")
        return acao, propriedade, objeto

    return acao, propriedade, objeto


def validar_comando(acao, propriedade, objeto):
    global acoes

    valido = False

    if acao and objeto:
        for acaoCadastrada in acoes:
            if acao == acaoCadastrada["nome"]:
                if propriedade in acaoCadastrada["propriedades"]:
                    if objeto in acaoCadastrada["objetos"]:
                        valido = True
                        break

    return valido


def executar_comando(acao, propriedade, objeto):
    responder_questao(propriedade, objeto)


def responder_questao(propriedade, objeto):
    global dicionarioValores

    resposta = "Não possuo resposta para esta questão."

    if dicionarioValores.get(objeto):
        propriedadeObjeto = dicionarioValores.get(
            objeto).geometricProperties.name
        if propriedadeObjeto == propriedade:
            resposta = "A " + propriedade + \
                " de um(a) " + objeto + " é " + \
                dicionarioValores.get(objeto).geometricProperties.formula

    print(resposta)

    return resposta


if __name__ == '__main__':
    iniciar()
    continuar = True
    while continuar:
        try:
            comando = escutar_comando()
            if comando:
                acao, propriedade, objeto = tokenizar_comando(comando)
                valido = validar_comando(acao, propriedade, objeto)
                if valido:
                    executar_comando(acao, propriedade, objeto)
                else:
                    print("Não entendi o comando. Repita, por favor!")
        except KeyboardInterrupt:
            print("Tchau!")
            continuar = False
